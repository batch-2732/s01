// 1. create array
let myArray = ["mors", "rafael", "morales"];
console.log("myArray = ", myArray);
// 2. access first character
let firstChar = myArray[0];
console.log("firstChar = ", firstChar);
// 3. last character
let lastChar = myArray[-1];
console.log("lastChar = ", lastChar);
// 4. return index of a value
let indexOfMors = myArray.indexOf("mors");
console.log("indexOfMors = ", indexOfMors);
// 5. loops over all elements of an array performing a user defind
myArray.forEach((item) => {
  console.log(item);
});
// 6. new array from elements obtained from user defined function ?MAP
let moreThanSixChars = myArray.map((item) => {
  return item.length >= 6;
});

console.log("moreThanSixChars = ", moreThanSixChars);

// 7. what array method checks if all elements satisfy

// answer is every()

// 8. what array method checks if at least one of its element

// answer is some()

// 9. splice() modifies copy of array leaving original unchanged (true or false)

// answer false. splice is mutator

// 10. slice ()...

// answeer true. slice is non mutator

// FUCNTION CODING

let students = ["John", "joe", "jane", "jessie"];
function addToEnd(myArray, element) {
  if (typeof element != "string") {
    console.log("error - can only add strings to an array");
  } else {
    myArray.push(element);
    console.log(myArray);
  }
}
addToEnd(students, "ryan");
addToEnd(students, 045);
function addToStart(myArray, element) {
  if (typeof element != "string") {
    console.log("error - can only add strings to an array");
  } else {
    myArray.unshift(element);
    console.log(myArray);
  }
}
addToStart(students, "Tess");
addToStart(students, 033);
console.log("coding #3");
console.log(students);
function elementChecker(myArray2, element) {
  if (myArray2.length == 0) {
    console.log("error - passed in array is empty");
  } else {
    return myArray2.some((item) => {
      item == element;
    });
  }
}
console.log(elementChecker(students, "jane"));
console.log(elementChecker([], "jane"));

console.log("coding #4");
function checkAllStringsEnding(myArray, myChar) {
  if (myArray.length == 0) {
    return "error - array must NOT be empty";
  } else if (
    myArray.some((element) => {
      typeof element != "string";
    })
  ) {
    return " error - all array elements must be strings";
  } else if (typeof myChar != "string") {
    return "error - 2nd argument must be a string";
  } else if (myChar.length != 1) {
    return "error - 2nd argument must be single charracter";
  } else {
    return myArray.every((element) => {
      element[element.length - 1] == myChar;
    });
  }
}
console.log(checkAllStringsEnding(students, "e"));
console.log(checkAllStringsEnding([], "e"));
console.log(checkAllStringsEnding(["Jane", 02], "e"));
console.log(checkAllStringsEnding(students, "el"));

console.log(checkAllStringsEnding(students, 4));
// console.log("coding #5");

// function stringLengthSorter(myArray) {
//   let sortedArray = [];
//   myArray.forEach((element) => {
//     if (sortedArray == []) {
//       sortedArray.push(element);
//     } else {
//       sortedArray.forEach((element2)=>{
//         if (element2.length <= element)
//       })
//     }
//   });
// }
