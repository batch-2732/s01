let students = ["mors", "jc", "john"];
console.log(typeof students);

// review
students.push("elmer");
console.log(students);

let students2 = {
  1: "mors",
  2: "jc",
  3: "john",
};
console.log(students2[1]);

// push pag end... unshift pag start ilalagay
// pop and shift naman ung pairs

// whats the difference between slice and splice\
// slice is non mutator
// splice is mutator

let numbers = [15, 20, 23, 30, 37];

numbers.forEach((number) => {
  if (number % 5 == 0) {
    console.log(number + " is divisible by 5");
  } else {
    console.log(number + " is not divisible by 5");
  }
});

let div5 = numbers.every((number) => {
  number < 40;
});
console.log(div5);

// 1. create array
let myArray = ["mors", "rafael", "morales"];
console.log("myArray = ", myArray);
// 2. access first character
let firstChar = myArray[0];
console.log("firstChar = ", firstChar);
// 3. last character
let lastChar = myArray[myArray.length - 1];
console.log("lastChar = ", lastChar);
// 4. return index of a value
let indexOfMors = myArray.indexOf("mors");
console.log("indexOfMors = ", indexOfMors);
// 5. loops over all elements of an array performing a user defind
myArray.forEach((item) => {
  console.log(item);
});
// 6. new array from elements obtained from user defined function ?MAP
let moreThanSixChars = myArray.filter((item) => {
  return item.length >= 6;
});

console.log("moreThanSixChars = ", moreThanSixChars);

// 7. what array method checks if all elements satisfy

// answer is every()

// 8. what array method checks if at least one of its element

// answer is some()

// 9.
