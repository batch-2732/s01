// 1. create array
let myArray = ["mors", "rafael", "morales"];
console.log("myArray = ", myArray);
// 2. access first character
let firstChar = myArray[0];
console.log("firstChar = ", firstChar);
// 3. last character
let lastChar = myArray[-1];
console.log("lastChar = ", lastChar);
// 4. return index of a value
let indexOfMors = myArray.indexOf("mors");
console.log("indexOfMors = ", indexOfMors);
// 5. loops over all elements of an array performing a user defind
myArray.forEach((item) => {
  console.log(item);
});
// 6. new array from elements obtained from user defined function ?MAP
let moreThanSixChars = myArray.map((item) => {
  return item.length >= 6;
});

console.log("moreThanSixChars = ", moreThanSixChars);

// 7. what array method checks if all elements satisfy

// answer is every()

// 8. what array method checks if at least one of its element

// answer is some()

// 9. splice() modifies copy of array leaving original unchanged (true or false)

// answer false. splice is mutator

// 10. slice ()...

// answeer true. slice is non mutator

// FUCNTION CODING

let students = ["John", "joe", "jane", "jessie"];
function addToEnd(myArray, element) {
  if (typeof element != "string") {
    console.log("error - can only add strings to an array");
  } else {
    myArray.push(element);
    console.log(myArray);
  }
}
addToEnd(students, "ryan");
addToEnd(students, 045);
function addToStart(myArray, element) {
  if (typeof element != "string") {
    console.log("error - can only add strings to an array");
  } else {
    myArray.unshift(element);
    console.log(myArray);
  }
}
addToStart(students, "Tess");
addToStart(students, 033);
function elementChecker(myArray, element) {
  if (myArray == []) {
    console.log("error - passed in array is empty");
  } else {
    return myArray.some((item) => {
      item == element;
    });
  }
}
console.log(elementChecker(students, "jane"));
console.log(elementChecker([], "jane"));

function checkAllStringsEnding(myArray, myChar) {
  if (myArray == []) {
    return "error - array must NOT be empty";
  } else {
    console.log("to be continued");
  }
}
